import sqlite3

connection = sqlite3.connect('chinook.db')
cursor = connection.cursor()

query = '''
    SELECT
        albums.albumid,
        albums.title,
        artists.name,
        genres.name
    FROM
        artists
    JOIN
        albums
        ON albums.artistid = artists.artistid
    LEFT JOIN
        tracks
        ON tracks.albumid = albums.albumid
    LEFT JOIN
        genres
        ON genres.genreid = tracks.genreid
    WHERE
        artists.name LIKE ?
    ORDER BY
        albums.albumid, tracks.name
'''

def main():
    while True:
        zoekvraag = input('Naar welke artiest wil je zoeken? ')
        if not zoekvraag:
            return
        rows = zoek(zoekvraag)
        resultaten = analyseer(rows)
        geef_weer(zoekvraag, resultaten)


def zoek(zoekvraag: str) -> sqlite3.Cursor:
    results = cursor.execute(query, [f'%{zoekvraag}%'])
    return results

def analyseer(rows: sqlite3.Cursor) -> list:
    resultaten = []
    album_id = None
    for row in rows:
        if row[0] != album_id:
            album = {
                'title': row[1],
                'artist': row[2],
                'genres': {row[3]}
            }
            resultaten.append(album)
            album_id = row[0]
        else:
            album['genres'].add(row[3])
    return resultaten

def geef_weer(zoekvraag: str, resultaten: list) -> None:
    print(f'Voor {zoekvraag} is het volgende gevonden:')
    for resultaat in resultaten:
        print(resultaat['title'], '-', resultaat['artist'], '-', resultaat['genres'])

if __name__ == '__main__':
    main()
