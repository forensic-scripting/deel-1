import sqlite3

connection = sqlite3.connect('chinook.db')
cursor = connection.cursor()

query = '''
    SELECT
        albums.title,
        artists.name,
        tracks.name
    FROM
        artists
    JOIN
        albums
        ON albums.artistid = artists.artistid
    LEFT JOIN
        tracks
        ON tracks.albumid = albums.albumid
    WHERE
        albums.title LIKE ? 
        OR tracks.name LIKE ? 
    ORDER BY
        albums.albumid, tracks.name
'''

def main():
    while True:
        zoekvraag = input('Naar welk woord wil je zoeken? ')
        if not zoekvraag:
            return
        rows = zoek(zoekvraag)
        for row in rows:
            print(row)

def zoek(zoekvraag: str) -> sqlite3.Cursor:
    zoekvraag = f'%{zoekvraag}%'
    results = cursor.execute(query, [zoekvraag, zoekvraag])
    return results

if __name__ == '__main__':
    main()
