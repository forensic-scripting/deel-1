class Telefoonboek:

    def __init__(self, filename):
        self.filename = filename

    def count_lines(self) -> int:
        """Count number of lines in file"""
        lines = 0
        with open(self.filename) as file:
            for line in file:
                lines += 1
        return lines

    def count_unique_names(self) -> int:
        """Count number of unique names in file"""
        namen = []
        with open(self.filename) as file:
            for line in file:
                parts = line.split(',')
                naam = parts[0] + ' ' + parts[1]
                namen.append(naam)
        unieke_namen = set(namen)
        return len(unieke_namen)

    def names_with_more_numbers(self) -> dict:
        """Return names with multiple numbers"""
        phonebook = self.create_phonebook()
        names = {}
        for name in phonebook:
            numbers = phonebook[name]
            if len(numbers) > 1:
                print(name, phonebook[name])
                names[name] = numbers
        return names

    def create_phonebook(self) -> dict:
        """Return dict with name as key and list of numbers as value"""
        phonebook = {}
        with open(self.filename) as file:
            for line in file:
                voornaam, achternaam, nummer = line.split(',')
                naam = voornaam + ' ' + achternaam
                nummer = nummer.strip()
                if naam not in phonebook:
                    phonebook[naam] = [nummer]
                else:
                    phonebook[naam].append(nummer)
        return phonebook


