======
Deel 1
======
Variabelen
==========
Variabelen zijn namen voor gegevens. Gegevens zijn objecten met eigenschappen (properties) en
methodes (methods).

Belangrijk
  Zorg dat je *altijd* weet wat het type is van het object waar je variabele naar verwijst.

Object types
------------
+---------------------------+----------+--------------------------------------+----------+---------+--------+
|                           |type      |voorbeeld                             |not true  |sequence |mutable |
+===========+===============+==========+======================================+==========+=========+========+
|number     |integer        |``int``   |``10``, ``12345``, ``-3``             |``0``     |         |        |
|           +---------------+----------+--------------------------------------+----------+---------+--------+
|           |floating point |``float`` |``0.1``, ``-2.5``, ``3.14159265359``  |``0.0``   |         |        |
+-----------+---------------+----------+--------------------------------------+----------+---------+--------+
|           |string         |``str``   |``'abc'``, ``"Don't feed the lama."`` |``''``    |ja       |        |
+-----------+---------------+----------+--------------------------------------+----------+---------+--------+
|collection |tuple          |``tuple`` |``(0, 1, 2, 3, 2)``                   |``()``    |ja       |        |
|           +---------------+----------+--------------------------------------+----------+---------+--------+
|           |list           |``list``  |``['a', 'b', 'c']``                   |``[]``    |ja       |ja      |
|           +---------------+----------+--------------------------------------+----------+---------+--------+
|           |dictionary     |``dict``  |``{'naam': 'Linda', 'leeftijd': 10}`` |``{}``    |         |ja      |
|           +---------------+----------+--------------------------------------+----------+---------+--------+
|           |set            |``set``   |``{'Aap', 'Noot', 'Mies'}``           |``set()`` |         |ja      |
+-----------+---------------+----------+--------------------------------------+----------+---------+--------+
|           |boolean        |``bool``  |``True``, ``False``                   |``False`` |         |        |
+-----------+---------------+----------+--------------------------------------+----------+---------+--------+
|           |None           |``None``  |``None``                              |``None``  |         |        |
+-----------+---------------+----------+--------------------------------------+----------+---------+--------+

``int``
  Een geheel getal, positief of negatief.

``float``
  Een getal met een deel achter de "komma". Als je een ``int`` deelt door een ``int`` is het
  resultaat altijd een ``float``::

    10 / 8
    ► 1.25

  Hou er rekening mee dat een ``float`` intern als binaire waarde wordt verwerkt. Dit kan een
  kleine afwijking opleveren ten opzichte van decimale stelsel. Meestal geen probleem maar
  wanneer je resultaten van berekeningen met elkaar gaat vergelijken, kun je tegen een
  onverwacht effect aan lopen::

    1.1 + 2.2
    ► 3.3000000000000003

  Voor meer uitleg: `Decimal vs float`_ door Phil Best of `Floating Point Arithmetic`_ in de
  Python Documentation.

``str``
  Een reeks letters, cijfers en leestekens tussen enkele of dubbele aanhalingstekens.

  Een string kan speciale tekens bevatten zoals ``\t`` voor een tab en ``\n`` voor een
  newline. Voor de lengte van de string tellen deze als een enkel teken.

``tuple``
  Een reeks objecten (gegevens), gescheiden door komma's. De objecten mogen van elk type zijn,
  hebben een vaste volgorde en kunnen aangewezen worden met hun positie (index), beginnend bij
  ``0``.

``list``
  Hetzelfde als een ``tuple`` maar mutable: objecten in een ``list`` kunnen gewijzigd,
  aangevuld of verwijderd worden.

``dict``
  Een reeks key-value paren. Hetzelfde als een ``list`` maar elk object in een ``dict`` heeft
  een unieke naam (key) in plaats van een positie.

  De key mag alleen een immutable object zijn, dus bijvoorbeeld een ``int``, ``str`` of
  ``tuple`` maar geen ``list``. De key mag binnen de ``dict`` maar één keer voorkomen.

``set``
  Een unieke verzameling immutable objecten. De regels voor de key in een ``dict`` gelden voor
  de waardes in een ``set``.

  De objecten in een ``set`` hebben geen vaste volgorde en kunnen dus niet op positie (index)
  of naam (key) gevonden worden. Maar je kunt wel ontzettend snel opzoeken of een object in
  een ``set`` voorkomt en de overeenkomsten of verschillen tussen twee (grote) sets bepalen.

``bool``
  ``True`` of ``False``

``None``
  ``None``

Assignment
----------
Je wijst een waarde toe aan een variabele met het ``=``-teken::

  programmeertaal = 'Python'

Namen van variabelen schrijf je in kleine letters, met underscores tussen woorden. Geef
variabelen een duidelijke naam. Gebruik geen afkortingen.

Het heeft de voorkeur Engelse namen aan variabelen (en functies en classes) te geven. Ik
gebruik in de voorbeelden Nederlandse namen om het onderscheid tussen namen en code duidelijk
te maken.

Access
------
Je gebruikt de variabele om het object waar de variabele naar verwijst te gebruiken::

    leeftijd = 10
    print(leeftijd)

is hetzelfde als::

    print(10)

Van een sequence (``string``, ``tuple`` en ``list``) kun je elk deel aanroepen door de positie
(index) tussen vierkante haken te zetten::

    'abcde'[0]          ► 'a'
    'abcde'[2]          ► 'c'
    ('x', 42, True)[1]  ► 42
    ['Jan', 'Kees'][0]  ► 'Jan'

De waardes in een ``dict`` vraag je op met de naam (key) tussen vierkante haken::

    leeftijd = {'Flip': 31, 'Mirjam': 27, 'Elise': 49}
    naam = 'Mirjam'
    leeftijd[naam]      ► 27

Op die manier kun je ook een waarde toevoegen aan een dict::

    leeftijd['Klaas'] = 15
    print(leeftijd)     ► {'Flip': 31, 'Mirjam': 27, 'Elise': 49, 'Klaas': 15}

Slicing
-------
Lees de blog over slicing_ van Sergii Boiko

Programma structuren
====================
Elke programmeertaal kent de volgende structuren:

- sequential
- selection
- loop

Bekijk het filmpje op gfcglobal.org_ voor uitleg over deze concepten.

Sequential
----------
Opdrachten worden stuk voor stuk, achter elkaar uitgevoerd::

    diameter = 7
    pi = 3.14
    oppervlakte = diameter * diameter * pi
    print('De oppervlakte is', oppervlakte)

Selection
---------
Er wordt een keuze gemaakt welke opdrachten moeten worden uitgevoerd::

    seizoen = input("Wat is het seizoen?")

    if seizoen == 'winter':
        print('Het vriest')

    elif seizoen == 'zomer':
        print('De zon schijnt.')
        print('Kom achter die computer vandaan en ga naar buiten!')

    else:
        print('Het regent.')

    print('Fijne dag verder.')

Als de vergelijking achter ``if`` of ``elif`` waar is, wordt de code in het betreffende
ingesprongen blok uitgevoerd, anders de code in het blok na ``else``. De laatste, niet
ingesprongen, regel maakt weer deel uit van het gewone sequentiële proces dus wordt altijd
uitgevoerd.

Let op:
  De regel voorafgaand aan een blok eindigt altijd met een ``:``. Blokken laat je inspringen
  met vier spaties.

Loop
----
Python kent twee loop structuren: de ``for``-loop en de ``while``-loop.

In een ``while``-loop wordt een blok steeds opnieuw uitgevoerd zolang de vergelijking waar is. ::

  teller = 0

  while teller < 10:
      teller = teller + 1
      print(teller)

  print('Wie niet weg is, is gezien!')

Met een ``for``-loop wordt het blok uitgevoerd voor elke waarde die ``in`` een sequence of
collection voorkomt. Dit wordt een "iteration" genoemd. ::

    kompas = ['noord', 'oost', 'zuid', 'west']

    for windrichting in kompas:
        print(windrichting)

    print('Thuis best.')

Bij de iteration over een string wordt de loop voor elk karakter uit die string uitgevoerd.
Bij een ``tuple``, ``list`` of ``set`` voor elk object in de collection. Bij een ``dict`` voer
je de loop uit met elke key uit de ``dict``, de bijbehorende value kun je in de loop opvragen::

    kleuren = {'lente': 'geel', 'zomer': 'groen', 'herfst': 'rood', 'winter': 'blauw'}

    for seizoen in kleuren:
        # seizoen is dus achtereenvolgens 'lente', 'zomer', 'herfst' en 'winter'
        print('De kleur van de', seizoen, 'is', kleuren[seizoen])

Lees uit bestand
================
Gebruik altijd de constructie ``with open() as`` om een bestand te openen::

    with open('item_1.txt') as bestand:
        for regel in bestand:
            print(regel)

.. _Decimal vs float: https://blog.teclado.com/decimal-vs-float-in-python/
.. _Floating Point Arithmetic: https://docs.python.org/3/tutorial/floatingpoint.html
.. _gfcglobal.org: https://edu.gcfglobal.org/en/computer-science/sequences-selections-and-loops/1/
.. _slicing: https://railsware.com/blog/python-for-machine-learning-indexing-and-slicing-for-lists-tuples-strings-and-other-sequential-types/