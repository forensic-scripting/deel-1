import sqlite3

connection = sqlite3.connect('chinook.db')
cursor = connection.cursor()

query = '''
    SELECT
        albums.albumid,
        albums.title,
        artists.name,
        tracks.name
    FROM
        artists
    JOIN
        albums
        ON albums.artistid = artists.artistid
    LEFT JOIN
        tracks
        ON tracks.albumid = albums.albumid
    WHERE
        artists.name LIKE ?
    ORDER BY
        albums.albumid, tracks.name
'''

def main():
    while True:
        zoekvraag = input('Naar welke artiest wil je zoeken? ')
        if not zoekvraag:
            return
        rows = zoek(zoekvraag)
        resultaten(zoekvraag, rows)


def zoek(zoekvraag: str) -> sqlite3.Cursor:
    results = cursor.execute(query, [f'%{zoekvraag}%'])
    return results


def resultaten(zoekvraag: str, rows: sqlite3.Cursor) -> None:
    print(f'Voor {zoekvraag} is het volgende gevonden:')
    albumid = None
    for row in rows:
        if albumid != row[0]:
            print(row[1] + ' - ' + row[2])
            albumid = row[0]
        print('\t' + row[3])

if __name__ == '__main__':
    main()
