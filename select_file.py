import os

files = []
for file in os.listdir():
    if file.endswith('.txt'):
        files.append(file)

print('Files in directory:')

for i, file in enumerate(files):
    print(i, file)

choice = input('Welke file wil je analyseren? ')

print('Analyseer file', files[int(choice)])
