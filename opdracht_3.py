__version__ = '1.0.0'

infile ='item_1.txt'
outfile = 'rapport.txt'


def main():
    aantal_regels = count_lines(filename=infile)
    unieke_namen = count_unique_names(filename=infile)
    namen_met_meer_nummers = names_with_more_numbers(filename=infile)

    write_report(
        infile=infile,
        outfile=outfile,
        lines=aantal_regels,
        unique=unieke_namen,
        multi=namen_met_meer_nummers
    )


def count_lines(filename: str) -> int:
    """Count number of lines in file"""
    lines = 0
    with open(filename) as file:
        for line in file:
            lines += 1
    return lines


def count_unique_names(filename: str) -> int:
    """Count number of unique names in file"""
    namen = []
    with open(filename) as file:
        for line in file:
            parts = line.split(',')
            naam = parts[0] + ' ' + parts[1]
            namen.append(naam)
    unieke_namen = set(namen)
    return len(unieke_namen)


def names_with_more_numbers(filename: str) -> dict:
    """Return names with multiple numbers"""
    phonebook = create_phonebook(filename)
    names = {}
    for name in phonebook:
        numbers = phonebook[name]
        if len(numbers) > 1:
            print(name, phonebook[name])
            names[name] = numbers
    return names


def create_phonebook(filename: str) -> dict:
    """Return dict with name as key and list of numbers as value"""
    phonebook = {}
    with open(filename) as file:
        for line in file:
            voornaam, achternaam, nummer = line.split(',')
            naam = voornaam + ' ' + achternaam
            nummer = nummer.strip()
            if naam not in phonebook:
                phonebook[naam] = [nummer]
            else:
                phonebook[naam].append(nummer)
    return phonebook


def most_used_givennames(filename: str) -> dict:
    """Return aprox 10 most used givennames"""
    names = {}
    with open(filename) as file:
        for line in file:
            name = line.split(',')[0]
            if name not in names:
                names[name] = 1
            else:
                names[name] += 1

    top_10_aantallen = sorted(names.values())[-10:]
    top_10_names = {}

    for name, count in names.items():
        if count in top_10_aantallen:
            top_10_names[name] = count

    # for name in names:
    #     if names[name] in top_10_aantallen:
    #         top_10_names[name] = names[name]

    return top_10_names


def write_report(
    infile: str,
    outfile: str,
    lines: int,
    unique: int,
    multi: dict
):
    """Write report to outfile"""
    with open(outfile, 'w') as file:
        file.write(f'RAPPORT\n\n{__file__} versie {__version__}\n\nOnderzocht bestand: {infile}\n\n')
        file.write(f'Aantal regels in het bestand: {lines}\n')
        file.write(f'Aantal unieke namen in het bestand: {unique}\n')
        file.write('Namen met meer dan één nummer:\n')
        for name in multi:
            file.write(f'\t{name}\t{multi[name]}\n')


if __name__ == '__main__':
    main()