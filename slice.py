"""
namen = ['Aap', 'Noot', 'Mies', 'Wim']

Opdracht:
Zorg dat de namen uit de lijst als volgt worden weergegeven:

De namen in de lijst zijn: Aap, Noot, Mies en Wim.
"""
namen = ['Aap', 'Noot', 'Mies', 'Wim']

print('De namen in de lijst zijn: ', end='')

for naam in namen[:-2]:
    print(naam + ', ', end='')

print(namen[-2], 'en', namen[-1] + '.')
