import logging
import sys

log = logging.getLogger()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(formatter)
log.addHandler(handler)

filehandler = logging.FileHandler('logging.txt')
filehandler.setFormatter(formatter)
log.addHandler(filehandler)

log.setLevel(logging.DEBUG)


log.info('open file "item_1.txt"')
with open('item_1.txt') as file:
    log.debug('file is open')
    log.info('read lines')
    for line in file:
        log.debug('line: %s', line.strip())